#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COLS 80
#define ROWS 60

struct st_char
{
  char c,row,column;
};

void init()
{
  puts("%!PS-Adobe-2.0");
  puts("%%Creator: /usr/bin/vi");
  puts("%%Title: Tempus fugit");
  puts("%%CreationDate: Tue May 29 22:38:46 2006");
  puts("%%Pages: (atend)");
  puts("%%PageOrder: Ascend");
  puts("%%BoundingBox: 0 0 612 792");
  puts("%%EndComments");
  puts("%%Page: 1 1");
  puts("/tms {/Times-Roman findfont 10 scalefont setfont} def");
  puts("/hlv {/Helvetica findfont 30 scalefont setfont} def");
  puts("/cou {/Courier findfont 10 scalefont setfont} def");
  puts("/hlvi {/Helvetica-Oblique findfont 20 scalefont setfont} def");
  puts("/sg {setgray} bind def");
  puts("/red {1 0 0 setrgbcolor} def");
  puts("/blue {0 0 1 setrgbcolor} def");
  puts("/lw {setlinewidth} bind def");
  puts("/mt {moveto} bind def");
  puts("/lt {lineto} bind def");
  puts("/rlt {rlineto} bind def");
  puts("/ff {showpage} def");
  puts("\ncou");
}

void showpage(struct st_char *page)
{
  int i,j,r;
  struct st_char character;

  for (i=0; i<ROWS; ++i)
  {
    for (j=0; j<COLS; ++j)
    {
       character=page[i+ROWS*j];
       printf("%%%% %d %d %d\n",i,j,i+ROWS*j);
       /*printf("%d %d mt\n",20+10*page[i+ROWS+j].column,
              700-10*page[i+ROWS+j].row);
       fprintf("(%c) show\n",page[i+ROWS+j].c);*/
       if (character.c)
       {
         printf("%d %d mt\n",20+10*character.column,
                700-10*character.row);
         printf("(%c) show\n",character.c); /* Escapen! */ 
       }
    }
  }
  puts("ff");
}

void finalize(int pages)
{
  puts("ff");
  puts("%%EndDocument");
  printf("%%%%Pages: %d\n",pages);
}

main(int argc,char *argv[])
{
  char c;
  int i,j;
  FILE *fp;
  struct st_char page[ROWS*COLS];

  init();
  for (i=0; i<ROWS; ++i)
  {
    for (j=0; j<COLS; ++j)
    {
      page[i+ROWS*j].c=' '+i%95;
      page[i+ROWS*j].row=i;
      page[i+ROWS*j].column=j;
    }
  }
  showpage(page);
  finalize(1);
}
